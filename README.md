# ZachIR's LARBS-Equivalent Setup Script (ZIRLESS)
# LARBS: Luke's Auto-Rice Bootstrapping Scripts

## Q: Isn't this just LARBS?

A: This is a fork of LARBS, with my programs and config stuff set as the
default. The two scripts will likely diverge over time.

For example, my 'progs.csv' assumes running Artix with Runit as the init
system, unlike LARBS, which is agnostic.

## Installation:

On Artix (with runit as init), run the following as root:

```
sh zirless.sh
```

That's it.

## What is ZIRLESS

ZIRLESS is a script that autoinstalls and autoconfigures a fully-functioning
and minimal terminal-and-vim-based Arch Linux environment.

ZIRLESS can be run on a fresh install of Artix Linux, and provides you with a
fully configured diving-board for work or more customization.

NOTE: ZIRLESS expects the following packages, metapackages, and groups to be
installed:

- base
- git
- a linux kernel, plus its headers
- the runit init system (unless you provide your own progs.csv)
- a network connection and manager (dhcpcd, wpa_supplicant, networkmanager, etc)

ZIRLESS also assumes the following about the other files:

- progs.csv should have AUR packages after any optional build dependencies
- if the users dotfiles repo needs to install anything into the home dir, it
  should have a Makefile, and `make install` should install these files (or the
  user should expect to do it manually).

## Customization

By default, ZIRLESS uses the programs [here in progs.csv](progs.csv) and installs
[my dotfiles repo (dotfiles) here](https://gitlab.com/zachir/dotfiles),
but you can easily change this by either modifying the default variables at the
beginning of the script or giving the script one of these options:

- `-r`: custom dotfiles repository (URL)
- `-p`: custom programs list/dependencies (local file or URL)
- `-a`: a custom AUR helper (must be able to install with `-S` unless you
  change the relevant line in the script

### The `progs.csv` list

ZIRLESS will parse the given programs list and install all given programs. Note
that the programs file must be a three column `.csv`.

The first column is a "tag" that determines how the program is installed, ""
(blank) for the main repository, `A` for via the AUR or `G` if the program is a
git repository that is meant to be `make && sudo make install`ed.

The second column is the name of the program in the repository, or the link to
the git repository, and the third column is a description (should be a verb
phrase) that describes the program. During installation, ZIRLESS will print out
this information in a grammatical sentence. It also doubles as documentation
for people who read the CSV and want to install my dotfiles manually.

Depending on your own build, you may want to tactically order the programs in
your programs file. ZIRLESS will install from the top to the bottom.

If you include commas in your program descriptions, be sure to include double
quotes around the whole description to ensure correct parsing.

### The script itself

The script is extensively divided into functions for easier readability and
trouble-shooting. Most everything should be self-explanatory.

The main work is done by the `installationloop` function, which iterates
through the programs file and determines based on the tag of each program,
which commands to run to install it. You can easily add new methods of
installations and tags as well.

Note that programs from the AUR can only be built by a non-root user. What
ZIRLESS does to bypass this by default is to temporarily allow the newly created
user to use `sudo` without a password (so the user won't be prompted for a
password multiple times in installation). This is done ad-hocly, but
effectively with the `newperms` function. At the end of installation,
`newperms` removes those settings, giving the user the ability to run only
several basic sudo commands without a password (`shutdown`, `reboot`,
`pacman -Syu`).
